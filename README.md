# HCM - Natural Language Processing
## Obsah repozitáře
Součástí tohoto repozitáře jsou:
* oanotovaný [dataset](./full-dataset.csv) sentimentu 1330 novinových článků ze 14 různých médií napříč českým mediálním prostorem
* notebook obsahující experimenty s [NLTK a Flair frameworky](./DeepL_Flair_NLTK.ipynb) pro predikci sentimentu
* notebook obsahující experimenty s [SVM modelem](./Bag_of_Words_SVM.ipynb) natrénovaným na bag of words datech pro predikci sentimentu
* notebook obsahující experimenty s [BERT language modelem](./bert/BERT.ipynb) pro predikci sentimentu



Autoři ("Hekři myslí"): Dominik Chodounský, Jiří Hájek, Martin Vadlejch, Lukáš Jančička

![Hackerman](./hackerman.jpg "Hackerman")
